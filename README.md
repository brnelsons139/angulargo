# AngularGo

# Get up and running
```bash
# Clone
git clone <THIS_REPO>

# Install deps for go
cd AngularGo/goServer
glide install

# Install deps for angular client
cd ../ngClient
npm i
```

# Create executable
```bash
cd ngClient
# Compile Angular Client
npm run build
# Compile Go Server
cd ../goServer
go build -o AngularGo.bin

# Execute AngularGo.bin to start server
```
