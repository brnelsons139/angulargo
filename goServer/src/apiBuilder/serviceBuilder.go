package apiBuilder

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

type Builder struct {
	router   *mux.Router
	basePath string
	port     int
	handler  http.Handler
}

func New(port int, rootHandler http.Handler) *Builder {
	return &Builder{
		router:   mux.NewRouter(),
		basePath: "/",
		port:     port,
		handler:  rootHandler,
	}
}

func (service *Builder) Handle(path string, handler http.Handler) *Builder {
	service.router.Handle(path, handler)
	return service
}

func (service *Builder) HandleFunc(path string, handler http.HandlerFunc) *Builder {
	service.router.HandleFunc(path, handler)
	return service
}

func (service *Builder) ListenAndServe() error {
	// we have to register the base path so it doesn't catch all the api calls
	service.Handle(service.basePath, service.handler)
	http.Handle(service.basePath, service.router)
	log.Printf("Listening on port %d...", service.port)
	portString := strconv.Itoa(service.port)
	port := ":" + portString
	log.Println(port)
	return http.ListenAndServe(port, nil)
}
