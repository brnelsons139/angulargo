package main

import (
	"encoding/json"
	"goServer/src/apiBuilder"
	"log"
	"net/http"
)

func main() {
	fileServer := http.FileServer(http.Dir("static"))
	err := apiBuilder.New(3000, fileServer).
		HandleFunc("/api/test", Test).
		ListenAndServe()
	if err != nil {
		log.Println(err)
	}
}

func Test(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case "GET":
		_ = json.NewEncoder(writer).
			Encode(map[string]bool{"ok": true})
		break
	}
}
